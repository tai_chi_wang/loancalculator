//
//  debugViewController.h
//  loanCalculator
//
//  Created by eala on 2015/3/3.
//  Copyright (c) 2015年 HwangDynChi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoanBook.h"

@interface debugViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *totalPrincipalLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPaymentLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalInterestLabel;
@property (weak, nonatomic) IBOutlet UILabel *installmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *hasPaidLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextPrincipalLabel;

@property (weak, nonatomic) IBOutlet UITableView *installmentsTableView;

@property (weak, nonatomic) LoanBook *selectedLoanBook;
@property int totalPrincipal;
@property int installment;
@property double rate;  // fixme later, it should be stored as array
@property (weak, nonatomic) NSDate *startDate;

@end
