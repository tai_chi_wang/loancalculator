//
//  Theme.h
//  loanCalculator
//
//  Created by Hank  on 2016/6/18.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Theme : NSObject
- (UIColor *)navigationBarTintColor;
- (UIColor *)navigationBarBarTintColor;
- (NSDictionary *)navigationBarTitleTextAttributes;

- (UIColor *)tabBarTintColor;
- (UIColor *)tabBarBarTintColor;
- (UIColor *)tabBarBackgroundColor;
- (BOOL)tabBarTranslucent;

- (NSDictionary *)tabBarItemTitleTextAttributes;
@end
