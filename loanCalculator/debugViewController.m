//
//  debugViewController.m
//  loanCalculator
//
//  Created by eala on 2015/3/3.
//  Copyright (c) 2015年 HwangDynChi. All rights reserved.
//

#import "debugViewController.h"
#import "installmentItems.h"
#import "Utility.h"
#import "EachInstallmentCell.h"
#import "AppDelegate.h"
#import "LoanContents.h"
#import "LoanBook.h"

@interface debugViewController ()
@property (nonatomic) int totalInterests;
@property (nonatomic) int totalPayment;

@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *loanContentFetchResultController;
@end

@implementation debugViewController

#pragma mark - getter & setter
- (NSManagedObjectContext *)managedObjectContext
{
    if (!_managedObjectContext) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _managedObjectContext = appDelegate.managedObjectContext;
    }
    return _managedObjectContext;
}

#pragma mark - init & reset
- (void)initFetch
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"belongToLoanBook.providedBank.name contains %@ && belongToLoanBook.type.name contains %@", self.selectedLoanBook.providedBank.name, self.selectedLoanBook.type.name];
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"belongToLoanBook = %@", self.selectedLoanBook.objectID];
    fetchRequest.predicate = predicate;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"LoanContent" inManagedObjectContext:self.managedObjectContext];
    
    NSSortDescriptor *installmentNoSortDesc = [[NSSortDescriptor alloc] initWithKey:@"installmentNo" ascending:YES];
    fetchRequest.sortDescriptors = @[installmentNoSortDesc];
    fetchRequest.entity = entityDesc;
    
    self.loanContentFetchResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil /*cacheName:@"loanContentCache"*/ cacheName:nil];
    self.loanContentFetchResultController.delegate = self;
    
    NSError *fetchError = nil;
    if ([self.loanContentFetchResultController performFetch:&fetchError]) {
        NSLog(@"Successfully fetch data from loanContent");
    }else{
        NSLog(@"Failed to fetch data from loanContent");
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initFetch];
    [self initInterface];
}

- (void)initInterface
{
    int rowIndex = [self.selectedLoanBook.nextInstallment intValue]-1;
    const int ALL_PAID = -1;
    if(ALL_PAID != rowIndex){
        rowIndex = MIN(rowIndex, (int)[self.loanContentFetchResultController.fetchedObjects count]-1);
        [self.installmentsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rowIndex inSection:0]
                                          atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    }
    
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [numberFormatter setMaximumFractionDigits: 0];
    
    self.totalPrincipalLabel.text = [numberFormatter stringFromNumber: self.selectedLoanBook.principal];
    self.totalInterestLabel.text = [numberFormatter stringFromNumber: self.selectedLoanBook.totalInterests];
    self.totalPaymentLabel.text = [numberFormatter stringFromNumber: [NSNumber numberWithInt:[self.selectedLoanBook.principal intValue] + [self.selectedLoanBook.totalInterests intValue]]];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    self.installmentLabel.text = [numberFormatter stringFromNumber: self.selectedLoanBook.installments];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY/MM/dd"];
    self.startDateLabel.text = [formatter stringFromDate:self.selectedLoanBook.startDate];
    self.endDateLabel.text = [formatter stringFromDate:self.selectedLoanBook.endDate];
    self.hasPaidLabel.text = [Utility moneyFormat:[self.selectedLoanBook.hasPaid intValue]];
    self.nextPrincipalLabel.text = [Utility moneyFormat:[self.selectedLoanBook.nextPrincipal intValue]];
}

#pragma mark - table view delegation & data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.loanContentFetchResultController sections] count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.loanContentFetchResultController.sections objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"installmentCell";
    EachInstallmentCell *cell = (EachInstallmentCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    
    if(nil == cell){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"installmentCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    LoanContents *loanContent = [self.loanContentFetchResultController objectAtIndexPath:indexPath];
    
    cell.installmentLabel.text = [loanContent.installmentNo stringValue];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY/MM/dd"];
    cell.dateLabel.text = [formatter stringFromDate:loanContent.date];
    
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [numberFormatter setMaximumFractionDigits: 0];
    
    cell.principalLabel.text = [numberFormatter stringFromNumber: loanContent.principal];
    cell.paymentLabel.text = [numberFormatter stringFromNumber: loanContent.payment];
    cell.installmentInterestLabel.text = [numberFormatter stringFromNumber: loanContent.installmentInterest];
    [numberFormatter setNumberStyle: NSNumberFormatterPercentStyle];
    [numberFormatter setMaximumFractionDigits: 2];
    cell.interestLabel.text = [numberFormatter stringFromNumber: loanContent.hasRate.rate];

    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    cell.installmentLabel.text = [numberFormatter stringFromNumber: loanContent.installmentNo];
    
    int nextInstallment = [self.selectedLoanBook.nextInstallment intValue];
    int cellInstallment = [loanContent.installmentNo intValue];
    
    if( -1 == nextInstallment){
        cell.backgroundColor = [UIColor lightGrayColor];
    }else{
        if(cellInstallment < nextInstallment){
            cell.backgroundColor = [UIColor lightGrayColor];
        }else if(cellInstallment == nextInstallment){
            //cell.backgroundColor = [UIColor colorWithRed:237.0/255 green:114.0/255 blue:89.0/255 alpha:1.0f];
            //cell.backgroundColor = [UIColor colorWithRed:237.0/255 green:200.0/255 blue:111.0/255 alpha:1.0f];
            //cell.backgroundColor = [UIColor brownColor];
            cell.backgroundColor = [UIColor colorWithRed:28.0/255 green:199.0/255 blue:121.0/255 alpha:1.0f];
            //UIColor *otherColor = [UIColor colorWithRed:255.0/255 green:136.0/255 blue:101.0/255 alpha:1.0f];
            UIColor *otherColor = [UIColor blackColor];
            cell.installmentLabel.textColor = otherColor;
            cell.principalLabel.textColor = otherColor;
            cell.installmentInterestLabel.textColor = otherColor;
            cell.interestLabel.textColor = otherColor;
        }else{
            cell.backgroundColor = [UIColor whiteColor];
        
            UIColor *otherColor = [UIColor blackColor];
            cell.installmentLabel.textColor = otherColor;
            cell.principalLabel.textColor = otherColor;
            cell.installmentInterestLabel.textColor = otherColor;
            cell.interestLabel.textColor = otherColor;
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 66;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *cellID = @"headerCell";
    
    EachInstallmentCell *cell = (EachInstallmentCell *)[self.installmentsTableView dequeueReusableCellWithIdentifier:cellID];
    
    if(nil == cell){
        cell = [[EachInstallmentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

@end
