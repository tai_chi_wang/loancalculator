//
//  SettingTableViewController.m
//  smartLoan
//
//  Created by Hank  on 2016/7/4.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import "SettingTableViewController.h"
//#import <DropboxSDK/DropboxSDK.h>

@interface SettingTableViewController ()

@end

@implementation SettingTableViewController

enum eSettingSections{
    E_SECTION_FEEDBACK = 0,
    E_SECTION_BACKUP,
    E_SECTION_ALL
};

enum eFeedbackItems{
    E_Email = 0,
    E_FEEDBACK_ALL
};

enum eBackup{
    E_Dropbox = 0,
    E_BACKUP_ALL
};

#pragma mark - view controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initInterface];
}

#pragma mark - init
- (void)initInterface
{
    self.clearsSelectionOnViewWillAppear = NO;
}

#pragma mark - delegate & data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return E_SECTION_ALL;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == E_SECTION_FEEDBACK){
        return E_FEEDBACK_ALL;
    }else if (section == E_SECTION_BACKUP){
        return E_BACKUP_ALL;
    }
    return 0;
}

- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    NSIndexPath *feedbackIndexPath = [NSIndexPath indexPathForRow:E_Email inSection:E_SECTION_FEEDBACK];
    NSIndexPath *dropboxIndexPath = [NSIndexPath indexPathForRow:E_Dropbox inSection:E_SECTION_BACKUP];
    
    if (indexPath == feedbackIndexPath) {
        if([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mailComposeVC = [[MFMailComposeViewController alloc] init];
            mailComposeVC.mailComposeDelegate = self;        // Required to invoke mailComposeController when send
            
            [mailComposeVC setSubject: NSLocalizedString(@"[SmartLoan] 意見回饋: ", @"feedback mail title")];
            [mailComposeVC setToRecipients:[NSArray arrayWithObject:@"scagile.tech@gmail.com"]];
            [mailComposeVC setMessageBody:@"" isHTML:NO];
            
            [self presentViewController:mailComposeVC animated:YES completion:nil];
        }
    }else if(dropboxIndexPath == indexPath){
        //[self performSegueWithIdentifier:@"dropboxSegue" sender:nil];
        //if (![[DBSession sharedSession] isLinked]) {
        //    [[DBSession sharedSession] linkFromController: self];
        //}
    }
}

#pragma mark - MFMail delegation
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}
@end
