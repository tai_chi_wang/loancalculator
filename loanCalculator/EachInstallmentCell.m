//
//  EachInstallmentCell.m
//  loanCalculator
//
//  Created by Hank  on 2015/3/8.
//  Copyright (c) 2015年 HwangDynChi. All rights reserved.
//

#import "EachInstallmentCell.h"

@implementation EachInstallmentCell

@synthesize installmentLabel = _installmentLabel;
@synthesize dateLabel = _dateLabel;
@synthesize principalLabel = _principalLabel;
@synthesize interestLabel = _interestLabel;
@synthesize paymentLabel = _paymentLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
