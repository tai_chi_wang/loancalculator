//
//  InterestRate+CoreDataProperties.h
//  smartLoan
//
//  Created by Hank  on 2016/7/9.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "InterestRate.h"
#import "LoanContents.h"

NS_ASSUME_NONNULL_BEGIN

@interface InterestRate (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *rate;
@property (nullable, nonatomic, retain) NSDate *startDate;
@property (nullable, nonatomic, retain) NSNumber *startInstallment;
@property (nullable, nonatomic, retain) LoanBook *belongToLoanBook;
@property (nullable, nonatomic, retain) LoanContents *belongToLoanItem;

@end

NS_ASSUME_NONNULL_END
