//
//  LoanContents+CoreDataProperties.h
//  smartLoan
//
//  Created by Hank  on 2016/7/9.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LoanContents.h"
#import "InterestRate.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoanContents (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSNumber *installmentInterest;
@property (nullable, nonatomic, retain) NSNumber *installmentNo;
@property (nullable, nonatomic, retain) NSNumber *leftPrincipal;
@property (nullable, nonatomic, retain) NSNumber *payment;
@property (nullable, nonatomic, retain) NSNumber *principal;
@property (nullable, nonatomic, retain) LoanBook *belongToLoanBook;
@property (nullable, nonatomic, retain) InterestRate *hasRate;

@end

NS_ASSUME_NONNULL_END
