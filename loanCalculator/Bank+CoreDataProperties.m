//
//  Bank+CoreDataProperties.m
//  smartLoan
//
//  Created by Hank  on 2016/7/9.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Bank+CoreDataProperties.h"

@implementation Bank (CoreDataProperties)

@dynamic id;
@dynamic name;
@dynamic provides;

@end
