//
//  Bank+CoreDataProperties.h
//  smartLoan
//
//  Created by Hank  on 2016/7/9.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Bank.h"

NS_ASSUME_NONNULL_BEGIN

@interface Bank (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSSet<LoanBook *> *provides;

@end

@interface Bank (CoreDataGeneratedAccessors)

- (void)addProvidesObject:(LoanBook *)value;
- (void)removeProvidesObject:(LoanBook *)value;
- (void)addProvides:(NSSet<LoanBook *> *)values;
- (void)removeProvides:(NSSet<LoanBook *> *)values;

@end

NS_ASSUME_NONNULL_END
