//
//  Theme.m
//  loanCalculator
//
//  Created by Hank  on 2016/6/18.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import "Theme.h"

@implementation Theme

- (UIColor *)navigationBarTintColor
{
    return [UIColor blackColor];
}

- (UIColor *)navigationBarBarTintColor
{
    return [UIColor grayColor];
}

- (NSDictionary *)navigationBarTitleTextAttributes
{
    return nil;
}

- (UIColor *)tabBarTintColor
{
    return [UIColor blackColor];
}

- (UIColor *)tabBarBarTintColor
{
    return [UIColor blackColor];
}

- (UIColor *)tabBarBackgroundColor
{
    return [UIColor grayColor];
}

- (BOOL)tabBarTranslucent
{
    return NO;
}

- (NSDictionary *)tabBarItemTitleTextAttributes
{
    return nil;
}

@end
