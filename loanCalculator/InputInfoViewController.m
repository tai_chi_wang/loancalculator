//
//  InputInfoViewController.m
//  loanCalculator
//
//  Created by Hank  on 2015/3/3.
//  Copyright (c) 2015年 HwangDynChi. All rights reserved.
//

#import "InputInfoViewController.h"
#import "debugViewController.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "LoanBook.h"
#import "Constants.h"
#import "InterestRate.h"
#import "UIView+Toast.h"

@interface InputInfoViewController ()
@property (weak, nonatomic) IBOutlet UITextField *typeTextField;
@property (weak, nonatomic) IBOutlet UITextField *bankNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *principalTextField;
@property (weak, nonatomic) IBOutlet UITextField *installmentsTextField;
@property (weak, nonatomic) IBOutlet UITextField *rateTextField;
@property (weak, nonatomic) IBOutlet UITextField *feeTextField;
@property (weak, nonatomic) IBOutlet UIDatePicker *startDatePicker;

@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) NSMutableArray *loanTypes;
@property (nonatomic) int paymentEachInstall;
@property (nonatomic) int paymentOnlyPrincipal;
@property (nonatomic) int paymentInterest;

@end

@implementation InputInfoViewController

#pragma mark - viewController lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initInterface];
    [self initTouch];
}

#pragma mark - setter & getter
- (NSManagedObjectContext *)managedObjectContext
{
    if (!_managedObjectContext) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _managedObjectContext = appDelegate.managedObjectContext;
    }
    return _managedObjectContext;
}

- (NSMutableArray *)loanTypes
{
    if (!_loanTypes) {
        _loanTypes = [[NSMutableArray alloc] init];
    }
    return _loanTypes;
}

#pragma mark - init

- (void)initFetch
{
    [self.loanTypes removeAllObjects];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"LoanType"
                                 inManagedObjectContext: self.managedObjectContext];
    
    NSError *error = nil;
    NSArray *types = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (error) {
        [NSException raise:@"no loan type found" format:@"%@", [error localizedDescription]];
        return;
    }else{
        for (LoanType *type in types){
            [self.loanTypes addObject: type.name];
        }
    }
    [self.loanTypes addObject: @"新增"];
}

- (void)initInterface
{
    [self initFetch];
    self.downPicker = [[DownPicker alloc] initWithTextField:self.typeTextField
                                                   withData: self.loanTypes];
    [self.downPicker addTarget:self
                        action:@selector(loanTypeChanged:)
              forControlEvents:UIControlEventValueChanged];
    
    [self.downPicker setPlaceholder: NSLocalizedString(@"選擇貸款種類", @"select loan type")];
}

- (void)initTouch
{
    // dismiss keyboard when tap the lowest view layer
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tap:)];
    [self.view addGestureRecognizer:tapRecognizer];
}

#pragma mark - event handler
-(void)tap:(UIGestureRecognizer *)gestureRecog
{
    [self.view endEditing:YES];
}

- (IBAction)startCalculate:(UIButton *)sender
{
    LoanBook *newLoanBook = nil;
    // 1. check for exist
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"LoanBook"
                                 inManagedObjectContext: self.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"providedBank.name contains %@ && type.name contains %@", self.bankNameTextField.text, self.typeTextField.text];
    request.predicate = predicate;
    NSError *error = nil;
    NSArray *objs = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (error) {
        [NSException raise:@"no loan book found" format:@"%@", [error localizedDescription]];
        return;
    }
    if (objs.count > 0) {
        // there is an loan book with same bank and type exsisting. Use update method
        [self.view makeToast: [NSString stringWithFormat:@"您已加入過 %@ %@", self.bankNameTextField.text, self.typeTextField.text]
                    duration: 1.5
                    position:CSToastPositionCenter];
        return;
    }else {
        newLoanBook = [NSEntityDescription insertNewObjectForEntityForName: @"LoanBook"
                                                   inManagedObjectContext: self.managedObjectContext];
        if (!newLoanBook) {
            NSLog(@"Failed to create new loan book");
            return;
        }else{
            // 1. bank name
            NSFetchRequest *request = [[NSFetchRequest alloc]init];
            request.entity = [NSEntityDescription entityForName:@"Bank" inManagedObjectContext: self.managedObjectContext];
            predicate = [NSPredicate predicateWithFormat:@"name contains %@", self.bankNameTextField.text];
            request.predicate = predicate;
            NSArray *fetchBanks = [self.managedObjectContext executeFetchRequest:request error:&error];
            if (error) {
                [NSException raise:@"checking bank exists failed" format:@"%@", [error localizedDescription]];
                return;
            }
            
            if (fetchBanks.count > 0) {
                newLoanBook.providedBank = [fetchBanks objectAtIndex:0];
            }else{
                Bank *newBank = [NSEntityDescription insertNewObjectForEntityForName: @"Bank"
                                                            inManagedObjectContext: self.managedObjectContext];
                if(!newBank){
                    NSLog(@"Failed to create new Bank");
                    return;
                }else{
                    newBank.name = self.bankNameTextField.text;
                    newLoanBook.providedBank = newBank;
                }
            }
            
            // 2. type
            request.entity = [NSEntityDescription entityForName:@"LoanType"
                                         inManagedObjectContext: self.managedObjectContext];
            predicate = [NSPredicate predicateWithFormat:@"name contains %@", self.typeTextField.text];
            request.predicate = predicate;
            NSArray *fetchTypes = [self.managedObjectContext executeFetchRequest:request error:&error];
            if (error) {
                [NSException raise:@"checking loan type exists failed" format:@"%@", [error localizedDescription]];
                return;
            }
            
            if (fetchTypes.count > 0) {
                newLoanBook.type = [fetchTypes objectAtIndex:0];
            }else{
                LoanType *loanType = [NSEntityDescription insertNewObjectForEntityForName: @"LoanType"
                                                              inManagedObjectContext: self.managedObjectContext];
                loanType.name = self.typeTextField.text;
                newLoanBook.type = loanType;
            }
            
            // 3. rate
            const int RATE_COUNTS = 1;  // fixme later, load multiple from UI addition
            for(int i=0; i< RATE_COUNTS; ++i){
                InterestRate *interestRate = [NSEntityDescription insertNewObjectForEntityForName:@"InterestRate"
                                                                           inManagedObjectContext:self.managedObjectContext];
                interestRate.rate = [NSNumber numberWithDouble:[self.rateTextField.text doubleValue]/100.0];
                interestRate.startDate = [self.startDatePicker date];
                interestRate.startInstallment = [NSNumber numberWithInt: i+1];
                interestRate.belongToLoanBook = newLoanBook;
            }
            
            newLoanBook.principal = [NSNumber numberWithInt:[self.principalTextField.text intValue]];
            newLoanBook.installments = [NSNumber numberWithInt:[self.installmentsTextField.text intValue]];
            newLoanBook.fee = [NSNumber numberWithInt:[self.feeTextField.text intValue]];
            newLoanBook.startDate = [self.startDatePicker date];
            //newLoanBook.interest = [NSNumber numberWithDouble: [self.rateTextField.text doubleValue]/100.0];  // from percentage to real number
        }
    }
    
    NSError *saveErr = nil;
    if ([self.managedObjectContext save:&saveErr]){
        NSLog(@"[InputInfoViewController] - new loan book was created");
    }else{
        NSLog(@"fail to save new loan book!");
        return;
    }
    
    if(nil != newLoanBook)
    {
        [Utility calculateInstallmentsFrom:newLoanBook SavedTo:self.managedObjectContext];
        self.tabBarController.selectedIndex = TAB_LIST;
    }
}

- (void)loanTypeChanged: (id)downPicker
{
    NSString* selectedValue = [downPicker text];
    if ([selectedValue isEqualToString:@"新增"]) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"新增貸款種類"
                                                                                  message:@"請輸入新增之貸款種類"
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField){
            textField.placeholder = NSLocalizedString(@"新增貸款種類", @"新增貸款種類");
            //textField.secureTextEntry = YES;
         }];
        
        UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action){
                                                              
                                                              if(0){ // // check by self.downPicker data not database
                                                              }else{
                                                                  LoanType *loanType = [NSEntityDescription insertNewObjectForEntityForName: @"LoanType"
                                                                                                                     inManagedObjectContext: self.managedObjectContext];
                                                                  loanType.name = alertController.textFields.firstObject.text;
                                                                  NSError *saveErr = nil;
                                                                  if ([self.managedObjectContext save:&saveErr]) {
                                                                      NSLog(@"add new loan type successfullly");
                                                                  }else{
                                                                      NSLog(@"failed to add new type");
                                                                  }
                                                                  [self initInterface];
                                                                  const int INDEX_OFFSET = 2;   // 1: begins from 0, 1: 新增
                                                                  self.downPicker.selectedIndex = self.loanTypes.count - INDEX_OFFSET;
                                                              }
                                                          }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //Handel no, thanks button
                                       
                                   }];
        
        [alertController addAction:okButton];
        [alertController addAction:noButton];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}
@end
