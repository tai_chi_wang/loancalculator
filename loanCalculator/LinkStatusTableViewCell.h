//
//  LinkStatusTableViewCell.h
//  smartLoan
//
//  Created by Hank  on 2016/10/22.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkStatusTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (weak, nonatomic) IBOutlet UISwitch *isLinkedSwitch;

@end
