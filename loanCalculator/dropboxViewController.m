//
//  dropboxViewController.m
//  smartLoan
//
//  Created by Hank  on 2016/8/24.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import "dropboxViewController.h"
#import "AppDelegate.h"
#import <DropboxSDK/DropboxSDK.h>
#import "LinkStatusTableViewCell.h"

@interface dropboxViewController()<DBRestClientDelegate>
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) DBRestClient *restClient;
@property (weak, nonatomic) IBOutlet UITableView *fileListTable;

@property (strong, nonatomic) NSMutableArray *filesMetaData;

@property (strong, nonatomic) NSMutableArray *folderListMetaData;
@property (strong, nonatomic) NSMutableArray *coreDataFilesMetaData;
@property int nLoadedFiles;

@property (strong, nonatomic) NSString *userName;
@end

@implementation dropboxViewController

enum DROPBOX_SETTING{
    DROPBOX_ACCOUNT=0,
    DROPBOX_FILES,
    DROPBOX_SETTINGS
};

#pragma mark - setter & getter
- (NSMutableArray *)filesMetaData
{
    if (!_filesMetaData) {
        _filesMetaData = [[NSMutableArray alloc] init];
    }
    return _filesMetaData;
}

- (AppDelegate *)appDelegate
{
    if (!_appDelegate) {
        _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (DBRestClient *)restClient
{
    if (!_restClient) {
        NSArray *userIds = [DBSession sharedSession].userIds;
        if(0 != [userIds count]){
            _restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession] userId: userIds[0]];
        }else{
            _restClient = [[DBRestClient alloc] initWithSession: [DBSession sharedSession]];
        }
        _restClient.delegate = self;
    }
    return _restClient;
}

#pragma mark - view controller lifecycle
- (void)viewDidLoad
{
    self.fileListTable.delegate = self;
    self.fileListTable.dataSource = self;
    self.restClient.delegate = self;
    
    //Add observer to see the changes
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(isDropboxLinkedHandle:)
                                                 name:@"isDropboxLinked"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    if([[DBSession sharedSession] isLinked]){
        [self.restClient loadMetadata:@"/"];
        [self.restClient loadAccountInfo];
    }
    [self.fileListTable reloadData];
}

#pragma mark - notification handler
- (void)isDropboxLinkedHandle:(id)sender
{
    if ([[sender object] intValue]) {
        //is linked.
        [self.restClient loadMetadata:@"/"];
        [self.restClient loadAccountInfo];
    }
    else {
        //is not linked
        [self.filesMetaData removeAllObjects];
        [self resetUI];
    }
}

#pragma mark - delegation
#pragma mark - Dropbox delegation - upload
- (void)restClient:(DBRestClient *)client uploadedFile:(NSString *)destPath
              from:(NSString *)srcPath metadata:(DBMetadata *)metadata
{
    NSLog(@"File uploaded successfully to path: %@", metadata.path);
    [self.restClient loadMetadata:@"/"];
}

- (void)restClient:(DBRestClient *)client uploadFileFailedWithError:(NSError *)error
{
    NSLog(@"File upload failed with error: %@", error);
}

#pragma mark - Dropbox delegation - load metadata
- (void)restClient:(DBRestClient *)client loadedMetadata:(DBMetadata *)metadata
{
    [self.filesMetaData removeAllObjects];
    if (metadata.isDirectory) {
        NSLog(@"Folder '%@' contains:", metadata.path);
        for (DBMetadata *file in metadata.contents) {
            NSLog(@"	%@", file.filename);
            [self.filesMetaData addObject:file];
        }
        [self.fileListTable reloadData];
    }
}

- (void)restClient:(DBRestClient *)client loadMetadataFailedWithError:(NSError *)error
{
    NSLog(@"Error loading metadata: %@", error);
}

#pragma mark - Dropbox delegation - download
- (void)restClient:(DBRestClient *)client loadedFile:(NSString *)destPath
       contentType:(NSString *)contentType metadata:(DBMetadata *)metadata
{
    NSLog(@"File loaded into path: %@", destPath);
    self.nLoadedFiles++;
    const int DB_FILES_COUNT = 3;
    if(DB_FILES_COUNT == self.nLoadedFiles){
        NSError *error = nil;
        NSURL *applicationDocumentsDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                                                       inDomains:NSUserDomainMask] lastObject];
        NSURL *storeURL = [applicationDocumentsDirectory URLByAppendingPathComponent: @"loanCalculator.sqlite"];
        if (![self.appDelegate.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                                       configuration:nil
                                                                                 URL:storeURL
                                                                             options:nil
                                                                               error:&error])
        {
            NSLog(@"failed to add db file, error %@, %@", error, [error userInfo]);
        }
        self.nLoadedFiles = 0;
    }
}

- (void)restClient:(DBRestClient *)client loadFileFailedWithError:(NSError *)error
{
    NSLog(@"There was an error loading the file: %@", error);
}

#pragma mark - Dropbox delegate -  load account info
- (void)restClient:(DBRestClient *)client loadedAccountInfo:(DBAccountInfo *)info
{
    self.userName = [info displayName];
    [self.fileListTable reloadData];
}

#pragma mark - table view data source
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == DROPBOX_ACCOUNT)
    {
        return @"帳號資訊";
    }
    else if(section == DROPBOX_FILES)
    {
        return @"備份資訊";
    }
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return DROPBOX_SETTINGS;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (DROPBOX_ACCOUNT == section) {
        return 1;
    }else if (DROPBOX_FILES == section){
        return [self.filesMetaData count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if(DROPBOX_FILES == indexPath.section){
        static NSString *cellID = @"dropboxFileNameCell";
        cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
        
        DBMetadata *metadata = [self.filesMetaData objectAtIndex:indexPath.row];
        cell.textLabel.text = metadata.filename;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        //cell.detailTextLabel.text = [NSString stringWithFormat:@"Backup Point %ld - %@", (long)indexPath.row, [formatter stringFromDate:metadata.lastModifiedDate]];
    }else if(DROPBOX_ACCOUNT == indexPath.section){
        static NSString *accountCellID = @"linkStatusCell";
        LinkStatusTableViewCell *cell = (LinkStatusTableViewCell *)[tableView dequeueReusableCellWithIdentifier:accountCellID];
        
        [cell.isLinkedSwitch setOn:[DBSession sharedSession].isLinked];
        cell.accountLabel.text = ([DBSession sharedSession].isLinked)? self.userName: @"帳號";

        return cell;
    }
    return cell;
}

#pragma mark - table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSArray *fileNames = @[@"loanCalculator.sqlite", @"loanCalculator.sqlite-wal", @"loanCalculator.sqlite-shm"];
    if(DROPBOX_FILES == indexPath.section){
        // reload from backup
        // 0. backup current first
        // 1. download from Dropbox
        // 2. replace local files, remove current, download into dir
        // 3. reset (reload) data store
        
        // check later if it affects indexPath order
        // 0. backup
        // fixme later, backup one version before restore
        //[self backup: @""];
        
        DBMetadata *metadata = [self.filesMetaData objectAtIndex:indexPath.row];
        NSString *dropboxDir = [NSString stringWithFormat:@"/%@", metadata.filename];
        NSString *localDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        
        NSError *error = nil;
        NSPersistentStore *persistentStore = [[self.appDelegate.persistentStoreCoordinator persistentStores] objectAtIndex:0];
        [self.appDelegate.persistentStoreCoordinator removePersistentStore:persistentStore error:&error];
        if (error) {
            NSLog(@"error removing persistent store %@ %@", error, [error userInfo]);
        }
        
        // remove
        NSArray *allFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:localDir
                                                                                error:nil];
        NSArray *coreDatafileNames = [allFiles filteredArrayUsingPredicate:
                                      [NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] 'loanCalculator'"]];
        for (NSString *fileName in coreDatafileNames){
            NSString *localPath = [localDir stringByAppendingPathComponent: fileName];
            [[NSFileManager defaultManager] removeItemAtPath:localPath error:&error];
            [self.restClient loadFile:[dropboxDir stringByAppendingPathComponent: fileName] intoPath:localPath];
        }
        // fixme later
        //[self backup: @""];
    }
}

#pragma mark - actions
- (IBAction)backupNow:(UIButton *)sender
{
    [self backup:@""];
}

- (IBAction)changeLinkStatus:(UISwitch *)sender
{
    if (sender.isOn) {
        if(![[DBSession sharedSession] isLinked])
            [[DBSession sharedSession] linkFromController: self];
        [self.fileListTable reloadData];
    }else{
        [[DBSession sharedSession] unlinkAll];
        [self.filesMetaData removeAllObjects];
        [self resetUI];
    }
}

#pragma mark - helper function
- (void)resetUI
{
    [self.filesMetaData removeAllObjects];
    self.userName = @"帳號";
    [self.fileListTable reloadData];
}

- (void)backup: (NSString *)prefix
{
    NSString *localDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSArray *allFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:localDir
                                                                            error:nil];
    
    NSArray *coreDatafileNames = [allFiles filteredArrayUsingPredicate:
                                  [NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] 'loanCalculator'"]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH-mm-ss"];
    NSString *destDir = [NSString stringWithFormat:@"/%@%@/", prefix, [formatter stringFromDate: [NSDate date]]];
    
    for (NSString *fileName in coreDatafileNames){
        NSString *localPath = [localDir stringByAppendingPathComponent: fileName];
        [self.restClient uploadFile:fileName toPath:destDir withParentRev:nil fromPath:localPath];
    }
    
    // replace
    //DBMetadata *metadata = [self.filesMetaData objectAtIndex:indexPath.row];
    //[self.restClient uploadFile:metadata.filename toPath:destDir withParentRev:metadata.rev fromPath:localPath];
}
@end
