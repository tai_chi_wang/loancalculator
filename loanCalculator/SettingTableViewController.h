//
//  SettingTableViewController.h
//  smartLoan
//
//  Created by Hank  on 2016/7/4.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface SettingTableViewController : UITableViewController<MFMailComposeViewControllerDelegate>

@end
