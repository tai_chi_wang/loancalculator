//
//  Constants.h
//  loanCalculator
//
//  Created by Hank  on 2016/6/11.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

enum TAB_IDX
{
    TAB_ADD = 0,
    TAB_LIST,
    TAB_SETTING,
    TAB_ALL
};

enum E_KEY
{
    KEY_DEFAULT_LOADED=0,
    KEY_ALL
};

extern NSString *const KEY_NAME[KEY_ALL];

#endif /* Constants_h */
