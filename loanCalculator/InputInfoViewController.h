//
//  InputInfoViewController.h
//  loanCalculator
//
//  Created by Hank  on 2015/3/3.
//  Copyright (c) 2015年 HwangDynChi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DownPicker.h"

@interface InputInfoViewController : UIViewController
@property(strong, nonatomic) DownPicker *downPicker;

@end
