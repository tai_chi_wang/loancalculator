//
//  loanListTableViewController.m
//  loanCalculator
//
//  Created by Hank  on 2016/6/5.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import "loanListTableViewController.h"
#import "loanItemTableViewCell.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "LoanBook.h"
#import "Bank.h"
#import "debugViewController.h"
#import "LoanContents.h"
#import "Utility.h"

@import GoogleMobileAds;

@interface loanListTableViewController ()
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;
@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *loanBookFetchResultController;
@property (strong, nonatomic) LoanBook *selectedLoanBook;
@property int monthlyPay;
@property int paidFee;
@end

@implementation loanListTableViewController

#pragma mark - init & reset
- (void)initFetch
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LoanBook"
                                              inManagedObjectContext:self.managedObjectContext];
    NSSortDescriptor *principalSort = [[NSSortDescriptor alloc]initWithKey:@"principal" ascending:NO];
    //NSSortDescriptor *dateSort = [[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:YES];
    //fetchRequest.sortDescriptors = @[principalSort, dateSort];
    fetchRequest.sortDescriptors = @[principalSort];
    [fetchRequest setEntity:entity];
    
    self.loanBookFetchResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                             managedObjectContext:self.managedObjectContext
                                                                               //sectionNameKeyPath:@"type"
                                                                               sectionNameKeyPath: nil
                                                                                        //cacheName:@"loanBookCache"];
                                                                                        cacheName:nil];
    
    self.loanBookFetchResultController.delegate = self;
    
    NSError *fetchError = nil;
    if ([self.loanBookFetchResultController performFetch:&fetchError]) {
        NSLog(@"Successfully fetched data from loanBook entity");
        self.monthlyPay = 0;
        self.paidFee = 0;
        
        for(LoanBook *loanBook in self.loanBookFetchResultController.fetchedObjects){
            self.monthlyPay += [loanBook.monthlyPay intValue];
            self.paidFee += [loanBook.fee intValue];
        }
    }else{
        NSLog(@"Failed to fetch any data from loanBook entity");
    }
}

#pragma mark - setter & getter
- (NSManagedObjectContext *)managedObjectContext
{
    if (!_managedObjectContext) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _managedObjectContext = appDelegate.managedObjectContext;
    }
    return _managedObjectContext;
}

#pragma mark - view life cycles
- (void)viewDidLoad
{
    [super viewDidLoad];

    // ad view
    self.bannerView.adUnitID = @"ca-app-pub-2988778406048560/2046876874";
    self.bannerView.rootViewController = self;
    [self.bannerView loadRequest:[GADRequest request]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self initFetch];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source & delegation
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.loanBookFetchResultController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.loanBookFetchResultController.sections objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"loanItemCell";
    
    loanItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID
                                                                  forIndexPath:indexPath];
    if(nil == cell){
        cell = [[loanItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:cellID];
    }
    
    LoanBook *loanBook = [self.loanBookFetchResultController objectAtIndexPath:indexPath];
    
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [numberFormatter setMaximumFractionDigits: 0];
    
    cell.bankNameLabel.text = loanBook.providedBank.name;
    cell.typeLabel.text = loanBook.type.name;
    const int TEN_THOUSAND = 10000;
    int tenThousandDollar = [loanBook.principal intValue] / TEN_THOUSAND;
    cell.principalLabel.text = [NSString stringWithFormat:@"$%d萬", tenThousandDollar];
    cell.paymentLabel.text = [Utility moneyFormat:[loanBook.monthlyPay intValue]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"YYYY-MM-dd"];
    [formatter setDateFormat:@"MM/dd"];
    if(loanBook.nextPayDate != nil){
        cell.payDateLabel.text = [formatter stringFromDate:loanBook.nextPayDate];
        cell.nextInstallmentLabel.text = [NSString stringWithFormat:@"%d/%d", [loanBook.nextInstallment intValue], [loanBook.installments intValue]];
    }else{
        cell.payDateLabel.text = @"已繳清";
        cell.nextInstallmentLabel.text = @"已繳清";
    }
    
    cell.leftLoanLabel.text = [NSString stringWithFormat:@"$%d萬", [loanBook.totalInterests intValue]/TEN_THOUSAND];
    
    [formatter setDateFormat:@"YYYY/MM"];
    cell.startDateLabel.text = [formatter stringFromDate: loanBook.startDate];
    cell.endDateLabel.text = [formatter stringFromDate: loanBook.endDate];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView  commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
                                             forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        LoanBook *loanBook = [self.loanBookFetchResultController objectAtIndexPath: indexPath];
        NSError *error = nil;
        [self.managedObjectContext deleteObject:loanBook];
        if ([self.managedObjectContext save:&error]){
            //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            NSLog(@"invoice was deleted");
            [tableView reloadData];
        }else {
            NSLog(@"Failed to delete the invoice, Error = %@", error);
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedLoanBook = [self.loanBookFetchResultController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"LOAN_CONTENT_SEGUE" sender:self];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"每月貸款總金額: %@  已支付手續費: %@", [[Utility class] moneyFormat:self.monthlyPay ], [[Utility class] moneyFormat:self.paidFee ]];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    
    header.textLabel.textColor = [UIColor blackColor];
    header.textLabel.font = [UIFont boldSystemFontOfSize:16];
    //header.textLabel.minimumFontSize = 9;
    [header.textLabel setMinimumScaleFactor:9.0/[UIFont labelFontSize]];
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentCenter;
}

#pragma mark - delegate - fetch controller delegate
- (void)controllerDidChangeContent: (NSFetchedResultsController *)controller
{
    //[self initFetch];
    [self.tableView reloadData];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"LOAN_CONTENT_SEGUE"]) {
        debugViewController *debugVC = [segue destinationViewController];
        debugVC.selectedLoanBook = self.selectedLoanBook;
    }
}

- (IBAction)addNewLoan:(UIBarButtonItem *)sender
{
    self.tabBarController.selectedIndex = TAB_ADD;
}
@end
