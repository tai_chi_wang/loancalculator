//
//  LoanBook.h
//  loanCalculator
//
//  Created by Hank  on 2016/6/12.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoanBook : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "LoanBook+CoreDataProperties.h"
