//
//  Utility.m
//  loanCalculator
//
//  Created by Hank  on 2015/3/8.
//  Copyright (c) 2015年 HwangDynChi. All rights reserved.
//

#import "Utility.h"
#import "LoanContents.h"
#import "InterestRate.h"
#import "Constants.h"

@import UIKit;
@import CoreData;

@implementation Utility

+ (NSString *)moneyFormat: (int)money
{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [numberFormatter setMaximumFractionDigits: 0];
    
    return [numberFormatter stringFromNumber: [NSNumber numberWithInt:money]];
}

+ (double)calcExp: (double)base with:(int) exponent
{
    double result = 1.0;
    int ex = exponent;
    while(ex > 0){
        result *= base;
        ex--;
    }
    return result;
}

+ (void)calculateInstallmentsFrom: (LoanBook *)loanBook
                          SavedTo: (NSManagedObjectContext *)managedObjectContext
{
    int totalPrincipal = [loanBook.principal intValue];
    int installments = [loanBook.installments intValue];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    request.entity = [NSEntityDescription entityForName:@"InterestRate"
                                 inManagedObjectContext:managedObjectContext];
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"belongToLoanBook.objectID contains %@", loanBook.objectID];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"belongToLoanBook.providedBank.name contains %@ && belongToLoanBook.type.name contains %@", loanBook.providedBank.name, loanBook.type.name];
    request.predicate = predicate;
    NSError *error = nil;
    NSArray *fetchedRates = [managedObjectContext executeFetchRequest:request
                                                                error:&error];
    if (error) {
        [NSException raise:@"fetch error" format:@"%@", [error localizedDescription]];
        return;
    }
    
    double interests = 0.0;
    InterestRate *interestRate = nil;
    if (fetchedRates.count > 0) {
        interestRate = [fetchedRates objectAtIndex:0];
        interests = [interestRate.rate doubleValue];
    }
    
    NSDate *startDate = loanBook.startDate;
    LoanContents *newLoanContent = [NSEntityDescription insertNewObjectForEntityForName:@"LoanContent"
                                                                 inManagedObjectContext:managedObjectContext];
    if (!newLoanContent) {
        NSLog(@"Failed to create new loan content!");
    }
    
    int totalInterests = 0;
    
    // first installment
    int leftPrincipal = totalPrincipal;
    int installmentNo = 1;
    int hasPaid = 0;
    NSDate *installmentDate = startDate;
    
    newLoanContent.belongToLoanBook = loanBook;
    newLoanContent.date = installmentDate;
    newLoanContent.installmentNo = [NSNumber numberWithInt: installmentNo];
    newLoanContent.hasRate = interestRate;
    newLoanContent.leftPrincipal = [NSNumber numberWithInt: totalPrincipal];
    
    const int MONTHS = 12;
    double monthInterest = interests/MONTHS;

    double monthExp = [self calcExp: (1.0+monthInterest) with:installments];
    double payBankRatio = ( monthExp * monthInterest)/(monthExp - 1.0);
    int payment = (int)(payBankRatio * leftPrincipal);
    newLoanContent.payment = [NSNumber numberWithInt: payment];
    int interest = leftPrincipal * monthInterest;
    newLoanContent.installmentInterest = [NSNumber numberWithInt:interest];
    int paidPrincipal = payment - interest;
    newLoanContent.principal = [NSNumber numberWithInt:paidPrincipal];
    
    BOOL needFindNext = TRUE;
    NSComparisonResult dateCompare = [newLoanContent.date compare: [NSDate date]];
    
    // Descending: newLoanContent.date is future of [NSDate date]
    if(dateCompare == NSOrderedDescending){
        needFindNext = FALSE;
        loanBook.nextPayDate = newLoanContent.date;
        loanBook.nextInstallment = [NSNumber numberWithInt:1];
        loanBook.hasPaid = [NSNumber numberWithInt:hasPaid];
    }
    
    hasPaid += payment;
    loanBook.monthlyPay = newLoanContent.payment;
    totalInterests += interest;
    
    // step
    leftPrincipal -= paidPrincipal;
    installmentNo += 1;
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *nextDate = [cal dateByAddingUnit:NSCalendarUnitMonth
                                       value:1
                                      toDate:newLoanContent.date
                                     options:0];
    
    for (int i=1; i< installments; ++i) {
        LoanContents *nextLoanContent = [NSEntityDescription insertNewObjectForEntityForName:@"LoanContent"
                                                                     inManagedObjectContext:managedObjectContext];
        if (!nextLoanContent) {
            NSLog(@"Failed to create new loan content!");
        }
        
        nextLoanContent.belongToLoanBook = loanBook;
        nextLoanContent.date = nextDate;
        nextLoanContent.installmentNo = [NSNumber numberWithInt: installmentNo];
        // fixme later, find the proper hasRate
        nextLoanContent.hasRate = interestRate;
        nextLoanContent.leftPrincipal = [NSNumber numberWithInt: leftPrincipal];
        
        nextLoanContent.payment = [NSNumber numberWithInt: payment];
        int interest = leftPrincipal * monthInterest;
        nextLoanContent.installmentInterest = [NSNumber numberWithInt:interest];
        int paidPrincipal = payment - interest;
        nextLoanContent.principal = [NSNumber numberWithInt:paidPrincipal];
        
        NSComparisonResult dateCompare = [nextLoanContent.date compare: [NSDate date]];
        
        if(needFindNext && dateCompare == NSOrderedDescending){
            needFindNext = FALSE;
            loanBook.nextPayDate = nextLoanContent.date;
            loanBook.nextInstallment = [NSNumber numberWithInt: installmentNo];
            loanBook.nextPrincipal = [NSNumber numberWithInt:leftPrincipal];
            loanBook.hasPaid = [NSNumber numberWithInt:hasPaid];
        }
        
        // step
        hasPaid += payment;
        leftPrincipal -= paidPrincipal;
        installmentNo += 1;
        nextDate = [cal dateByAddingUnit:NSCalendarUnitMonth
                                           value:1
                                          toDate:nextLoanContent.date
                                         options:0];
        totalInterests += interest;
    }
    
    if(needFindNext){   // all paid
        loanBook.nextPayDate = nil;
        loanBook.nextInstallment = 0;
    }
    
    loanBook.endDate = nextDate;
    loanBook.totalInterests = [NSNumber numberWithInt: totalInterests];
    
    NSError *saveErr = nil;
    if ([managedObjectContext save:&saveErr]) {
        NSLog(@"[Utility] - save all loan items");
    }else{
        NSLog(@"[Utility] - save loan items failed");
    }
}

+ (void)loadDefaultDataTo:(NSManagedObjectContext *)managedObjectContext
{
    // load default loanContent
    NSArray *loanTypeNames = @[
                               NSLocalizedString(@"房貸", @"房貸"),
                               NSLocalizedString(@"信貸", @"信貸"),
                               NSLocalizedString(@"車貸", @"車貸"),
                               NSLocalizedString(@"學貸", @"學貸")
                               ];
    for (NSString *loanTypeName in loanTypeNames){
        LoanType *newLoanType =
        [NSEntityDescription insertNewObjectForEntityForName:@"LoanType"
                                      inManagedObjectContext:managedObjectContext];
        newLoanType.name = loanTypeName;
    }
    
    NSError *error = nil;
    if ([managedObjectContext save:&error])
    {
        NSLog(@"default loan types have been loaded");
    }else {
        NSLog(@"Failed to load default types, Error = %@", error);
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool: YES
                   forKey: KEY_NAME[KEY_DEFAULT_LOADED]];
    [userDefaults synchronize];
}
@end