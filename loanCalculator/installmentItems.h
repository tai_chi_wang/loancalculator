//
//  installmentItems.h
//  loanCalculator
//
//  Created by Hank  on 2015/3/8.
//  Copyright (c) 2015年 HwangDynChi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface installmentItems : NSObject

@property int installment;
@property NSDate *date;
@property double rate;
@property int principal;
@property int interest;
@property int payment;
@property int leftPrincipal;

@end
