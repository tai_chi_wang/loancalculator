//
//  LoanBook+CoreDataProperties.m
//  smartLoan
//
//  Created by Hank  on 2016/7/9.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LoanBook+CoreDataProperties.h"

@implementation LoanBook (CoreDataProperties)

@dynamic endDate;
@dynamic fee;
@dynamic hasPaid;
@dynamic installments;
@dynamic monthlyPay;
@dynamic nextInstallment;
@dynamic nextPayDate;
@dynamic nextPrincipal;
@dynamic principal;
@dynamic startDate;
@dynamic totalInterests;
@dynamic containsRates;
@dynamic installmentContents;
@dynamic providedBank;
@dynamic type;

@end
