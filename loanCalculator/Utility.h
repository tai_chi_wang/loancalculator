//
//  Utility.h
//  loanCalculator
//
//  Created by Hank  on 2015/3/8.
//  Copyright (c) 2015年 HwangDynChi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoanBook.h"
@import CoreData;

@interface Utility : NSObject
+ (double)calcExp: (double)base with:(int) exponent;
+ (void)calculateInstallmentsFrom: (LoanBook *)data
                          SavedTo: (NSManagedObjectContext *)managedObjectContext;
+ (NSString *)moneyFormat: (int)money;
+ (void)loadDefaultDataTo: (NSManagedObjectContext *)managedObjectContext;
@end
