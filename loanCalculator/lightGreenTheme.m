//
//  lightGreenTheme.m
//  loanCalculator
//
//  Created by Hank  on 2016/6/18.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import "lightGreenTheme.h"

@implementation lightGreenTheme

- (UIColor *)mainColor
{
    return [UIColor colorWithRed:28.0/255 green:158.0/255 blue:121.0/255 alpha:1.0f];
}

- (NSString *)fontName
{
    return @"Avenir-Book";
}

- (NSString *)boldFontName
{
    return @"Avenir-Black";
}

#pragma mark - navigation bar
- (UIColor *)navigationBarTintColor
{
    return [UIColor whiteColor];
}

- (UIColor *)navigationBarBarTintColor
{
    return [self mainColor];
}

- (NSDictionary *)navigationBarTitleTextAttributes
{
    return @{NSForegroundColorAttributeName: [UIColor whiteColor],
             NSFontAttributeName : [UIFont fontWithName:[self boldFontName] size:18.0]};
}
@end
