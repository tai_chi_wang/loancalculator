//
//  LoanBook+CoreDataProperties.h
//  smartLoan
//
//  Created by Hank  on 2016/7/9.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LoanBook.h"
#import "InterestRate.h"
#import "LoanContents.h"
#import "Bank.h"
#import "LoanType.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoanBook (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *endDate;
@property (nullable, nonatomic, retain) NSNumber *fee;
@property (nullable, nonatomic, retain) NSNumber *hasPaid;
@property (nullable, nonatomic, retain) NSNumber *installments;
@property (nullable, nonatomic, retain) NSNumber *monthlyPay;
@property (nullable, nonatomic, retain) NSNumber *nextInstallment;
@property (nullable, nonatomic, retain) NSDate *nextPayDate;
@property (nullable, nonatomic, retain) NSNumber *nextPrincipal;
@property (nullable, nonatomic, retain) NSNumber *principal;
@property (nullable, nonatomic, retain) NSDate *startDate;
@property (nullable, nonatomic, retain) NSNumber *totalInterests;
@property (nullable, nonatomic, retain) NSSet<InterestRate *> *containsRates;
@property (nullable, nonatomic, retain) NSSet<LoanContents *> *installmentContents;
@property (nullable, nonatomic, retain) Bank *providedBank;
@property (nullable, nonatomic, retain) LoanType *type;

@end

@interface LoanBook (CoreDataGeneratedAccessors)

- (void)addContainsRatesObject:(InterestRate *)value;
- (void)removeContainsRatesObject:(InterestRate *)value;
- (void)addContainsRates:(NSSet<InterestRate *> *)values;
- (void)removeContainsRates:(NSSet<InterestRate *> *)values;

- (void)addInstallmentContentsObject:(LoanContents *)value;
- (void)removeInstallmentContentsObject:(LoanContents *)value;
- (void)addInstallmentContents:(NSSet<LoanContents *> *)values;
- (void)removeInstallmentContents:(NSSet<LoanContents *> *)values;

@end

NS_ASSUME_NONNULL_END
