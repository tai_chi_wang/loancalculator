//
//  LoanContents+CoreDataProperties.m
//  smartLoan
//
//  Created by Hank  on 2016/7/9.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LoanContents+CoreDataProperties.h"

@implementation LoanContents (CoreDataProperties)

@dynamic date;
@dynamic installmentInterest;
@dynamic installmentNo;
@dynamic leftPrincipal;
@dynamic payment;
@dynamic principal;
@dynamic belongToLoanBook;
@dynamic hasRate;

@end
