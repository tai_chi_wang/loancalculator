//
//  loanItemTableViewCell.h
//  loanCalculator
//
//  Created by Hank  on 2016/6/5.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface loanItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *principalLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftLoanLabel;

@property (weak, nonatomic) IBOutlet UILabel *nextInstallmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *payDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentLabel;

@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
@end
