//
//  LoanContents.h
//  smartLoan
//
//  Created by Hank  on 2016/6/18.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class LoanBook;

NS_ASSUME_NONNULL_BEGIN

@interface LoanContents : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "LoanContents+CoreDataProperties.h"
