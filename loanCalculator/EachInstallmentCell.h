//
//  EachInstallmentCell.h
//  loanCalculator
//
//  Created by Hank  on 2015/3/8.
//  Copyright (c) 2015年 HwangDynChi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EachInstallmentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *installmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *principalLabel;
@property (weak, nonatomic) IBOutlet UILabel *interestLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentLabel;
@property (weak, nonatomic) IBOutlet UILabel *installmentInterestLabel;
@end
