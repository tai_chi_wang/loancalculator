//
//  RoundCircle.m
//  loanCalculator
//
//  Created by Hank  on 2016/6/18.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import "RoundCircle.h"

@implementation RoundCircle

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawRect:(CGRect)rect
{
    CGRect borderRect = CGRectMake(0.0, 0.0, 60.0, 60.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(context, 1.0, 1.0, 1.0, 1.0);
    CGContextSetRGBFillColor(context, 28.0/255, 158.0/255, 121.0/255, 1.0);
    CGContextSetLineWidth(context, 2.0);
    CGContextFillEllipseInRect (context, borderRect);
    CGContextStrokeEllipseInRect(context, borderRect);
    CGContextFillPath(context);
}

@end
