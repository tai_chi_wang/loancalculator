//
//  loanListTableViewController.h
//  loanCalculator
//
//  Created by Hank  on 2016/6/5.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreData;

@interface loanListTableViewController : UITableViewController<NSFetchedResultsControllerDelegate>

@end
