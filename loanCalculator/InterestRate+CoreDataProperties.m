//
//  InterestRate+CoreDataProperties.m
//  smartLoan
//
//  Created by Hank  on 2016/7/9.
//  Copyright © 2016年 HwangDynChi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "InterestRate+CoreDataProperties.h"

@implementation InterestRate (CoreDataProperties)

@dynamic rate;
@dynamic startDate;
@dynamic startInstallment;
@dynamic belongToLoanBook;
@dynamic belongToLoanItem;

@end
